(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     35258,       1079]
NotebookOptionsPosition[     32144,       1015]
NotebookOutlinePosition[     32487,       1030]
CellTagsIndexPosition[     32444,       1027]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
    "res_", "1", " ", "atonal_dl", "_hh", " ", "n_", "4", "_l", "_", "10", 
     "_u", "_", "1"}], ",", "2", ",", "3", ",", "4", ",", "5", ",", "7", ",", 
    "8", ",", 
    RowBox[{"9", " ", "0.31288991258256227"}]}], "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{"Simplify", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"-", "0.680002"}], "*", 
     RowBox[{"(", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"(", 
          RowBox[{"(", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{"(", "HH", ")"}], "^", 
             RowBox[{"(", 
              RowBox[{"1", "/", "2"}], ")"}]}], ")"}], ")"}], ")"}], ")"}], 
        "^", "2"}], ")"}], ")"}]}], "-", 
    RowBox[{"3.42698", "*", 
     RowBox[{"(", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"DL", "-", 
          RowBox[{"(", 
           RowBox[{"-", "26.5346"}], ")"}]}], ")"}], "-", 
        RowBox[{
         RowBox[{"(", "HH", ")"}], "^", 
         RowBox[{"(", 
          RowBox[{"1", "/", "2"}], ")"}]}]}], ")"}], ")"}]}], "+", 
    RowBox[{"4.26969", "*", 
     RowBox[{"(", "0", ")"}]}], "+", 
    RowBox[{"4.5292", "*", 
     RowBox[{"(", 
      RowBox[{"DL", "-", 
       RowBox[{"(", 
        RowBox[{"-", "26.5346"}], ")"}]}], ")"}]}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.83256641551708*^9, 3.832566505863303*^9}},
 CellLabel->"In[17]:=",ExpressionUUID->"5762425e-08db-4ae7-8c51-85c494b56762"],

Cell[BoxData[
 RowBox[{"29.24696681200001`", "\[VeryThinSpace]", "+", 
  RowBox[{"1.1022200000000004`", " ", "DL"}], "+", 
  RowBox[{"3.42698`", " ", 
   SqrtBox["HH"]}], "-", 
  RowBox[{"0.680002`", " ", "HH"}]}]], "Output",
 CellChangeTimes->{3.8325664848824754`*^9, 3.8325671484327793`*^9},
 CellLabel->"Out[17]=",ExpressionUUID->"dac3aa63-4b14-4544-a9df-87b0a80aaf7b"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
    "res_", "2", " ", "atonal_dl", "_hh", " ", "n_", "2", "_l", "_", "20", 
     "_u", "_", "1"}], ",", "2", ",", "3", ",", "4", ",", 
    RowBox[{"5", " ", "0.3117182158664714"}]}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{"Simplify", "[", 
   RowBox[{
    RowBox[{"0.621889", "*", 
     RowBox[{"(", 
      RowBox[{"DL", "-", 
       RowBox[{"(", 
        RowBox[{"-", "63.5663"}], ")"}]}], ")"}]}], "-", 
    RowBox[{"0.423896", "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"HH", "-", 
         RowBox[{"(", "36.7715", ")"}]}], ")"}], "+", 
       RowBox[{"(", 
        RowBox[{"(", 
         RowBox[{"(", 
          RowBox[{"HH", "-", 
           RowBox[{"(", 
            RowBox[{"DL", "+", 
             RowBox[{"(", 
              RowBox[{"(", 
               RowBox[{"HH", "-", 
                RowBox[{"(", "36.7715", ")"}]}], ")"}], ")"}]}], ")"}]}], 
          ")"}], ")"}], ")"}]}], ")"}]}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.8325671551080537`*^9, 3.832567161520545*^9}},
 CellLabel->"In[18]:=",ExpressionUUID->"9dbcab22-48fc-45cc-bc80-183642ba2ed0"],

Cell[BoxData[
 RowBox[{"39.531182740700004`", "\[VeryThinSpace]", "+", 
  RowBox[{"1.045785`", " ", "DL"}], "-", 
  RowBox[{"0.423896`", " ", "HH"}]}]], "Output",
 CellChangeTimes->{3.8325671648279743`*^9},
 CellLabel->"Out[18]=",ExpressionUUID->"819999e8-0d33-49fa-895a-390c3da4ac1c"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
    "res_", "2", " ", "atonal_dl", "_hh", " ", "n_", "6", "_l", "_", "10", 
     "_u", "_", "1"}], ",", "2", ",", "3", ",", "4", ",", "5", ",", "7", ",", 
    "8", ",", 
    RowBox[{"9", " ", "0.31246045977470627"}]}], "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{"Simplify", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"-", "1.84607"}], "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"(", 
            RowBox[{"(", 
             RowBox[{
              RowBox[{"(", 
               RowBox[{
                RowBox[{"(", "DL", ")"}], "^", 
                RowBox[{"(", 
                 RowBox[{"1", "/", "2"}], ")"}]}], ")"}], "^", "2"}], ")"}], 
            ")"}], ")"}], "^", 
          RowBox[{"(", 
           RowBox[{"1", "/", "2"}], ")"}]}], ")"}], ")"}], "^", "2"}], 
      ")"}]}], "-", 
    RowBox[{"9.30055", "*", 
     RowBox[{"(", 
      RowBox[{"HH", "-", 
       RowBox[{"(", "51.0636", ")"}]}], ")"}]}], "-", 
    RowBox[{"8.43927", "*", 
     RowBox[{"(", "0", ")"}]}], "+", 
    RowBox[{"3.04372", "*", 
     RowBox[{
      RowBox[{"(", "HH", ")"}], "^", 
      RowBox[{"(", 
       RowBox[{"1", "/", "2"}], ")"}]}]}], "+", 
    RowBox[{"2.86956", "*", 
     RowBox[{"(", 
      RowBox[{"(", 
       RowBox[{"(", 
        RowBox[{"DL", "+", 
         RowBox[{"(", 
          RowBox[{"HH", "-", 
           RowBox[{"(", "51.0636", ")"}]}], ")"}]}], ")"}], ")"}], ")"}]}], 
    "+", 
    RowBox[{"5.86638", "*", 
     RowBox[{"(", 
      RowBox[{"HH", "-", 
       RowBox[{"(", "51.0636", ")"}]}], ")"}]}]}], "]"}]}]], "Input",
 CellLabel->"In[27]:=",ExpressionUUID->"4c6b3b12-ece4-4aa1-aa98-cf48a22a223f"],

Cell[BoxData[
 RowBox[{"28.83101919599997`", "\[VeryThinSpace]", "+", 
  RowBox[{"1.0234899999999998`", " ", "DL"}], "+", 
  RowBox[{"3.04372`", " ", 
   SqrtBox["HH"]}], "-", 
  RowBox[{"0.5646099999999992`", " ", "HH"}]}]], "Output",
 CellChangeTimes->{3.8325672308528986`*^9},
 CellLabel->"Out[27]=",ExpressionUUID->"6495afac-bc4e-4560-af63-d00a5b9b1c02"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
    "res_", "2", " ", "atonal_dl", "_hh", " ", "n_", "4", "_l", "_", "10", 
     "_u", "_", "1"}], ",", "2", ",", "3", ",", "4", ",", "5", ",", "6", ",", 
    "7", ",", "8", ",", "9", ",", "10", ",", 
    RowBox[{"11", " ", "0.31878287883316203"}]}], "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{"Simplify", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"-", "0.586504"}], "*", 
     RowBox[{"(", 
      RowBox[{"(", 
       RowBox[{"(", 
        RowBox[{"(", 
         RowBox[{"(", "HH", ")"}], ")"}], ")"}], ")"}], ")"}]}], "+", 
    RowBox[{"9.98537", "*", 
     RowBox[{"(", 
      RowBox[{"(", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{"(", 
             RowBox[{"1", "/", 
              RowBox[{"(", 
               RowBox[{"DL", "-", 
                RowBox[{"(", 
                 RowBox[{"-", "38.1942"}], ")"}]}], ")"}]}], ")"}], "*", 
            "HH"}], ")"}], ")"}], "^", "2"}], ")"}], ")"}], ")"}]}], "+", 
    RowBox[{"1.16496", "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"DL", "-", 
         RowBox[{"(", 
          RowBox[{"-", "38.1942"}], ")"}]}], ")"}], "*", 
       RowBox[{"Cos", "[", 
        RowBox[{"(", 
         RowBox[{"(", 
          RowBox[{"1", "/", 
           RowBox[{"(", 
            RowBox[{"DL", "-", 
             RowBox[{"(", 
              RowBox[{"-", "38.1942"}], ")"}]}], ")"}]}], ")"}], ")"}], 
        "]"}]}], ")"}]}], "-", 
    RowBox[{"9.9934", "*", 
     RowBox[{"(", 
      RowBox[{"Cos", "[", 
       RowBox[{"Sin", "[", 
        RowBox[{"(", 
         RowBox[{"(", 
          RowBox[{"(", 
           RowBox[{"(", 
            RowBox[{"HH", "*", 
             RowBox[{"(", "78.5058", ")"}]}], ")"}], ")"}], ")"}], ")"}], 
        "]"}], "]"}], ")"}]}]}], "]"}]}]], "Input",
 CellLabel->"In[28]:=",ExpressionUUID->"d42d2d39-1da7-4f8f-a22a-ec10918f884a"],

Cell[BoxData[
 RowBox[{
  RowBox[{"HH", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "0.586504`"}], "+", 
     FractionBox[
      RowBox[{"9.98537`", " ", "HH"}], 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"38.1942`", "\[VeryThinSpace]", "+", "DL"}], ")"}], "2"]]}], 
    ")"}]}], "+", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{"44.494715232000004`", "\[VeryThinSpace]", "+", 
     RowBox[{"1.16496`", " ", "DL"}]}], ")"}], " ", 
   RowBox[{"Cos", "[", 
    FractionBox["1", 
     RowBox[{"38.1942`", "\[VeryThinSpace]", "+", "DL"}]], "]"}]}], "-", 
  RowBox[{"9.9934`", " ", 
   RowBox[{"Cos", "[", 
    RowBox[{"Sin", "[", 
     RowBox[{"78.5058`", " ", "HH"}], "]"}], "]"}]}]}]], "Output",
 CellChangeTimes->{3.8325672641925087`*^9},
 CellLabel->"Out[28]=",ExpressionUUID->"b7ec6187-75e4-4f64-95ac-b1a0f9c09cc5"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
    "res_", "2", " ", "atonal_dl", "_hh", " ", "n_", "6", "_l", "_", "10", 
     "_u", "_", "1"}], ",", "2", ",", "3", ",", "4", ",", "5", ",", "6", ",", 
    "7", ",", "8", ",", "9", ",", "10", ",", "11", ",", "12", ",", 
    RowBox[{"13", " ", "0.3172968447926512"}]}], "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{"Simplify", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"-", "9.31845"}], "*", 
     RowBox[{"Exp", "[", 
      RowBox[{"(", 
       RowBox[{"DL", "*", 
        RowBox[{"(", 
         RowBox[{"-", "0.863438"}], ")"}]}], ")"}], "]"}]}], "-", 
    RowBox[{"0.209714", "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"HH", "-", 
           RowBox[{"(", "89.8265", ")"}]}], ")"}], "-", 
         RowBox[{"(", 
          RowBox[{"1", "/", 
           RowBox[{"(", 
            RowBox[{"DL", "-", 
             RowBox[{"(", 
              RowBox[{"-", "0.863438"}], ")"}]}], ")"}]}], ")"}]}], ")"}], 
       "+", "HH"}], ")"}]}], "+", 
    RowBox[{"10", "*", 
     RowBox[{"(", 
      RowBox[{"Sin", "[", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"(", 
          RowBox[{
           RowBox[{"(", 
            RowBox[{"(", 
             RowBox[{"(", 
              RowBox[{"(", 
               RowBox[{"HH", "*", 
                RowBox[{"(", "89.8265", ")"}]}], ")"}], ")"}], ")"}], ")"}], 
           "^", 
           RowBox[{"(", 
            RowBox[{"1", "/", "2"}], ")"}]}], ")"}], ")"}], "^", 
        RowBox[{"(", 
         RowBox[{"1", "/", "2"}], ")"}]}], "]"}], ")"}]}], "+", 
    RowBox[{"1.03207", "*", "DL"}], "+", 
    RowBox[{"7.96448", "*", 
     RowBox[{"(", 
      RowBox[{"Exp", "[", 
       RowBox[{"Exp", "[", 
        RowBox[{"(", 
         RowBox[{"(", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{"(", 
             RowBox[{"(", 
              RowBox[{"(", "0", ")"}], ")"}], ")"}], "^", "2"}], ")"}], ")"}],
          ")"}], "]"}], "]"}], ")"}]}], "+", 
    RowBox[{"0.202889", "*", 
     RowBox[{"(", "0", ")"}]}]}], "]"}]}]], "Input",
 CellLabel->"In[29]:=",ExpressionUUID->"02d6d98d-7561-41ab-b0a2-524c0cf70e09"],

Cell[BoxData[
 RowBox[{"40.4875758781255`", "\[VeryThinSpace]", "+", 
  RowBox[{"1.03207`", " ", "DL"}], "+", 
  FractionBox["0.209714`", 
   RowBox[{"0.863438`", "\[VeryThinSpace]", "+", "DL"}]], "-", 
  RowBox[{"9.31845`", " ", 
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{
     RowBox[{"-", "0.863438`"}], " ", "DL"}]]}], "-", 
  RowBox[{"0.419428`", " ", "HH"}], "+", 
  RowBox[{"10", " ", 
   RowBox[{"Sin", "[", 
    RowBox[{"3.078584791154792`", " ", 
     SuperscriptBox["HH", 
      RowBox[{"1", "/", "4"}]]}], "]"}]}]}]], "Output",
 CellChangeTimes->{3.8325672918138943`*^9},
 CellLabel->"Out[29]=",ExpressionUUID->"636aa9b1-5b1d-4621-aee9-eed6275f4195"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
    "res_", "3", " ", "melanogaster", " ", "atonal_dl", "_hh", " ", "n_", "4",
      "_l", "_", "10", "_u", "_", "1"}], ",", "2", ",", "3", ",", "4", ",", 
    "5", ",", "6", ",", "7", ",", "8", ",", "9", ",", "10", ",", "11", ",", 
    "12", ",", 
    RowBox[{"13", " ", "0.31547658346954044"}]}], "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{"Simplify", "[", 
   RowBox[{
    RowBox[{"9.78982", "*", 
     RowBox[{"Sin", "[", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"(", 
         RowBox[{"HH", "-", 
          RowBox[{"(", 
           RowBox[{"-", "27.6556"}], ")"}]}], ")"}], ")"}], "^", 
       RowBox[{"(", 
        RowBox[{"1", "/", "2"}], ")"}]}], "]"}]}], "-", 
    RowBox[{"4.80274", "*", 
     RowBox[{"(", 
      RowBox[{"(", 
       RowBox[{"DL", "/", 
        RowBox[{"Log", "[", 
         RowBox[{"(", 
          RowBox[{"(", 
           RowBox[{"1", "/", 
            RowBox[{"(", 
             RowBox[{"HH", "-", 
              RowBox[{"(", 
               RowBox[{"-", "27.6556"}], ")"}]}], ")"}]}], ")"}], ")"}], 
         "]"}]}], ")"}], ")"}]}], "+", 
    RowBox[{"5.27192", "*", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"(", 
        RowBox[{"Exp", "[", 
         RowBox[{"Exp", "[", 
          RowBox[{"(", 
           RowBox[{"Exp", "[", 
            RowBox[{"Sin", "[", 
             RowBox[{"(", 
              RowBox[{"1", "/", 
               RowBox[{"(", 
                RowBox[{"DL", "-", 
                 RowBox[{"(", 
                  RowBox[{"-", "92.9814"}], ")"}]}], ")"}]}], ")"}], "]"}], 
            "]"}], ")"}], "]"}], "]"}], ")"}], ")"}], "^", 
      RowBox[{"(", 
       RowBox[{"1", "/", "2"}], ")"}]}]}], "-", 
    RowBox[{"0.217659", "*", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{"Exp", "[", 
             RowBox[{"(", 
              RowBox[{"1", "/", 
               RowBox[{"(", 
                RowBox[{"DL", "-", 
                 RowBox[{"(", 
                  RowBox[{"-", "92.9814"}], ")"}]}], ")"}]}], ")"}], "]"}], 
            "*", "HH"}], ")"}], ")"}], "^", "2"}], ")"}], ")"}], "^", 
      RowBox[{"(", 
       RowBox[{"1", "/", "2"}], ")"}]}]}]}], "]"}]}]], "Input",
 CellLabel->"In[35]:=",ExpressionUUID->"252b5eaa-d275-4c90-80e4-950919d16650"],

Cell[BoxData[
 RowBox[{
  RowBox[{"5.27192`", " ", 
   SqrtBox[
    SuperscriptBox["\[ExponentialE]", 
     SuperscriptBox["\[ExponentialE]", 
      SuperscriptBox["\[ExponentialE]", 
       RowBox[{"Sin", "[", 
        FractionBox["1", 
         RowBox[{"92.9814`", "\[VeryThinSpace]", "+", "DL"}]], "]"}]]]]]}], 
  "-", 
  RowBox[{"0.217659`", " ", 
   SqrtBox[
    RowBox[{
     SuperscriptBox["\[ExponentialE]", 
      FractionBox["2", 
       RowBox[{"92.9814`", "\[VeryThinSpace]", "+", "DL"}]]], " ", 
     SuperscriptBox["HH", "2"]}]]}], "-", 
  FractionBox[
   RowBox[{"4.80274`", " ", "DL"}], 
   RowBox[{"Log", "[", 
    FractionBox["1", 
     RowBox[{"27.6556`", "\[VeryThinSpace]", "+", "HH"}]], "]"}]], "+", 
  RowBox[{"9.78982`", " ", 
   RowBox[{"Sin", "[", 
    SqrtBox[
     RowBox[{"27.6556`", "\[VeryThinSpace]", "+", "HH"}]], 
    "]"}]}]}]], "Output",
 CellChangeTimes->{3.832567332753409*^9},
 CellLabel->"Out[35]=",ExpressionUUID->"4a0cf354-3c9f-49c7-8229-5a618574179a"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
    "res_", "3", " ", "melanogaster", " ", "atonal_dl", "_hh", " ", "n_", "2",
      "_l", "_", "20", "_u", "_", "1"}], ",", "2", ",", "3", ",", "4", ",", 
    "5", ",", "6", ",", "7", ",", "8", ",", "9", ",", "10", ",", 
    RowBox[{"11", " ", "0.3110539573565275"}]}], "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{"Simplify", "[", 
   RowBox[{
    RowBox[{"0.631783", "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"1", "/", 
         RowBox[{"(", 
          RowBox[{"DL", "-", 
           RowBox[{"(", 
            RowBox[{"-", "26.5376"}], ")"}]}], ")"}]}], ")"}], "+", 
       RowBox[{"(", 
        RowBox[{"DL", "-", 
         RowBox[{"(", 
          RowBox[{"-", "26.5376"}], ")"}]}], ")"}]}], ")"}]}], "-", 
    RowBox[{"0.511438", "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{
          RowBox[{"(", "DL", ")"}], "^", 
          RowBox[{"(", 
           RowBox[{"1", "/", "2"}], ")"}]}], "*", 
         RowBox[{
          RowBox[{"(", "HH", ")"}], "^", 
          RowBox[{"(", 
           RowBox[{"1", "/", "2"}], ")"}]}]}], ")"}], "-", 
       RowBox[{"(", 
        RowBox[{"DL", "-", 
         RowBox[{"(", 
          RowBox[{"-", "26.5376"}], ")"}]}], ")"}]}], ")"}]}]}], 
   "]"}]}]], "Input",
 CellLabel->"In[36]:=",ExpressionUUID->"6a196b06-2975-4669-8325-a85d353778ca"],

Cell[BoxData[
 RowBox[{"30.3383416096`", "\[VeryThinSpace]", "+", 
  RowBox[{"1.143221`", " ", "DL"}], "+", 
  FractionBox["0.631783`", 
   RowBox[{"26.5376`", "\[VeryThinSpace]", "+", "DL"}]], "-", 
  RowBox[{"0.511438`", " ", 
   SqrtBox["DL"], " ", 
   SqrtBox["HH"]}]}]], "Output",
 CellChangeTimes->{3.832567355396058*^9},
 CellLabel->"Out[36]=",ExpressionUUID->"8056317a-4160-4cea-b911-7ca193d18bea"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
    "res_", "3", " ", "melanogaster", " ", "atonal_dl", "_hh", " ", "n_", "6",
      "_l", "_", "10", "_u", "_", "1"}], ",", "2", ",", "3", ",", "4", ",", 
    "5", ",", "7", ",", "8", ",", 
    RowBox[{"9", " ", "0.312955579630117"}]}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{"Simplify", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"-", "7.96459"}], "*", "DL"}], "+", 
    RowBox[{"9.79161", "*", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"(", 
        RowBox[{"(", 
         RowBox[{"(", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{"(", 
             RowBox[{"(", 
              RowBox[{"HH", "-", 
               RowBox[{"(", 
                RowBox[{"-", "100"}], ")"}]}], ")"}], ")"}], "^", 
            RowBox[{"(", 
             RowBox[{"1", "/", "2"}], ")"}]}], ")"}], ")"}], ")"}], ")"}], 
       ")"}], "^", 
      RowBox[{"(", 
       RowBox[{"1", "/", "2"}], ")"}]}]}], "+", 
    RowBox[{"9.0371", "*", "DL"}], "-", 
    RowBox[{"9.57694", "*", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"(", "DL", ")"}], "^", "2"}], ")"}], "^", 
        RowBox[{"(", 
         RowBox[{"1", "/", "2"}], ")"}]}], ")"}], "^", 
      RowBox[{"(", 
       RowBox[{"1", "/", "2"}], ")"}]}]}], "-", 
    RowBox[{"9.98325", "*", 
     RowBox[{"(", 
      RowBox[{"(", 
       RowBox[{"(", 
        RowBox[{"HH", "-", 
         RowBox[{"(", 
          RowBox[{
           RowBox[{"(", 
            RowBox[{"(", 
             RowBox[{"(", "DL", ")"}], ")"}], ")"}], "^", 
           RowBox[{"(", 
            RowBox[{"1", "/", "2"}], ")"}]}], ")"}]}], ")"}], ")"}], ")"}]}], 
    "+", 
    RowBox[{"9.47316", "*", "HH"}]}], "]"}]}]], "Input",
 CellLabel->"In[37]:=",ExpressionUUID->"b90a0a5f-05b5-4570-94bd-65b00d834bbb"],

Cell[BoxData[
 RowBox[{
  RowBox[{"9.98325`", " ", 
   SqrtBox["DL"]}], "+", 
  RowBox[{"1.0725100000000003`", " ", "DL"}], "-", 
  RowBox[{"9.57694`", " ", 
   SuperscriptBox[
    RowBox[{"(", 
     SuperscriptBox["DL", "2"], ")"}], 
    RowBox[{"1", "/", "4"}]]}], "-", 
  RowBox[{"0.5100899999999999`", " ", "HH"}], "+", 
  RowBox[{"9.79161`", " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"100", "+", "HH"}], ")"}], 
    RowBox[{"1", "/", "4"}]]}]}]], "Output",
 CellChangeTimes->{3.8325673798277326`*^9},
 CellLabel->"Out[37]=",ExpressionUUID->"dca80c72-baee-4b73-bf44-904060406abb"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
    "res_", "3", " ", "melanogaster", " ", "atonal_dl", "_hh", " ", "n_", "2",
      "_l", "_", "20", "_u", "_", "1"}], ",", "2", ",", "3", ",", "4", ",", 
    RowBox[{"5", " ", "0.31516201159486323"}]}], "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{"Simplify", "[", 
   RowBox[{
    RowBox[{"0.169982", "*", 
     RowBox[{"(", 
      RowBox[{"(", 
       RowBox[{"(", 
        RowBox[{"HH", "-", 
         RowBox[{"(", 
          RowBox[{"(", 
           RowBox[{"(", 
            RowBox[{"(", 
             RowBox[{"(", 
              RowBox[{"(", 
               RowBox[{"(", "DL", ")"}], ")"}], ")"}], ")"}], ")"}], ")"}], 
          ")"}]}], ")"}], ")"}], ")"}]}], "+", 
    RowBox[{"0.582259", "*", 
     RowBox[{"(", 
      RowBox[{"(", 
       RowBox[{"DL", "+", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"(", 
             RowBox[{
              RowBox[{"(", 
               RowBox[{"DL", "-", 
                RowBox[{"(", 
                 RowBox[{"-", "97.6457"}], ")"}]}], ")"}], "-", 
              RowBox[{"(", 
               RowBox[{"DL", "-", 
                RowBox[{"(", 
                 RowBox[{"-", "97.6457"}], ")"}]}], ")"}]}], ")"}], "-", 
            RowBox[{"(", 
             RowBox[{"HH", "-", 
              RowBox[{"(", 
               RowBox[{"-", "33.1451"}], ")"}]}], ")"}]}], ")"}], "+", 
          RowBox[{"(", 
           RowBox[{"(", 
            RowBox[{"DL", "-", 
             RowBox[{"(", 
              RowBox[{"-", "97.6457"}], ")"}]}], ")"}], ")"}]}], ")"}]}], 
       ")"}], ")"}]}]}], "]"}]}]], "Input",
 CellLabel->"In[38]:=",ExpressionUUID->"74120d0c-a50d-4858-ab81-d8dbedf70b61"],

Cell[BoxData[
 RowBox[{"37.5560548554`", "\[VeryThinSpace]", "+", 
  RowBox[{"0.994536`", " ", "DL"}], "-", 
  RowBox[{"0.412277`", " ", "HH"}]}]], "Output",
 CellChangeTimes->{3.832567422008995*^9},
 CellLabel->"Out[38]=",ExpressionUUID->"29d34aaa-9372-4a22-a1cf-1a44aa5c3bd5"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
    "res_", "3", " ", "simulans", " ", "atonal_dl", "_hh", " ", "n_", "6", 
     "_l", "_", "10", "_u", "_", "1"}], ",", "2", ",", "3", ",", "4", ",", 
    "5", ",", "6", ",", "7", ",", "8", ",", "9", ",", "10", ",", "11", ",", 
    "12", ",", 
    RowBox[{"13", " ", "0.24808778187260772"}]}], "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{"Simplify", "[", 
   RowBox[{
    RowBox[{"9.99503", "*", 
     RowBox[{"Sin", "[", 
      RowBox[{"Cos", "[", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"1", "/", 
           RowBox[{"(", 
            RowBox[{"DL", "-", 
             RowBox[{"(", 
              RowBox[{"-", "49.8317"}], ")"}]}], ")"}]}], ")"}], "*", 
         RowBox[{"(", 
          RowBox[{"HH", "-", 
           RowBox[{"(", 
            RowBox[{"-", "82.1523"}], ")"}]}], ")"}]}], ")"}], "]"}], "]"}]}],
     "+", 
    RowBox[{"10", "*", 
     RowBox[{"Cos", "[", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"HH", "-", 
            RowBox[{"(", 
             RowBox[{"-", "82.1523"}], ")"}]}], ")"}], "/", 
          RowBox[{"(", 
           RowBox[{"DL", "-", 
            RowBox[{"(", 
             RowBox[{"-", "49.8317"}], ")"}]}], ")"}]}], ")"}], ")"}], "^", 
       "2"}], "]"}]}], "+", 
    RowBox[{"2.49833", "*", 
     RowBox[{"(", "0", ")"}]}], "-", 
    RowBox[{"8.89356", "*", 
     RowBox[{"Sin", "[", 
      RowBox[{"(", 
       RowBox[{"1", "/", 
        RowBox[{"(", 
         RowBox[{"HH", "-", 
          RowBox[{"(", 
           RowBox[{"-", "82.1523"}], ")"}]}], ")"}]}], ")"}], "]"}]}], "-", 
    RowBox[{"0.173221", "*", "HH"}], "+", 
    RowBox[{"0.813371", "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"1", "/", 
         RowBox[{"(", 
          RowBox[{"HH", "-", 
           RowBox[{"(", 
            RowBox[{"-", "82.1523"}], ")"}]}], ")"}]}], ")"}], "+", 
       RowBox[{"(", 
        RowBox[{"DL", "-", 
         RowBox[{"(", 
          RowBox[{"-", "49.8317"}], ")"}]}], ")"}]}], ")"}]}]}], 
   "]"}]}]], "Input",
 CellLabel->"In[39]:=",ExpressionUUID->"591372c0-44ff-4828-85c8-990cb169ddc5"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"-", "0.173221`"}], " ", "HH"}], "+", 
  RowBox[{"0.813371`", " ", 
   RowBox[{"(", 
    RowBox[{"49.8317`", "\[VeryThinSpace]", "+", "DL", "+", 
     FractionBox["1", 
      RowBox[{"82.1523`", "\[VeryThinSpace]", "+", "HH"}]]}], ")"}]}], "+", 
  RowBox[{"10", " ", 
   RowBox[{"Cos", "[", 
    FractionBox[
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"82.1523`", "\[VeryThinSpace]", "+", "HH"}], ")"}], "2"], 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"49.8317`", "\[VeryThinSpace]", "+", "DL"}], ")"}], "2"]], 
    "]"}]}], "-", 
  RowBox[{"8.89356`", " ", 
   RowBox[{"Sin", "[", 
    FractionBox["1", 
     RowBox[{"82.1523`", "\[VeryThinSpace]", "+", "HH"}]], "]"}]}], "+", 
  RowBox[{"9.99503`", " ", 
   RowBox[{"Sin", "[", 
    RowBox[{"Cos", "[", 
     FractionBox[
      RowBox[{"82.1523`", "\[VeryThinSpace]", "+", "HH"}], 
      RowBox[{"49.8317`", "\[VeryThinSpace]", "+", "DL"}]], "]"}], 
    "]"}]}]}]], "Output",
 CellChangeTimes->{3.832567461093664*^9},
 CellLabel->"Out[39]=",ExpressionUUID->"25202c90-b754-44b3-9b05-142023a6fc26"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
    "res_", "3", " ", "simulans", " ", "atonal_dl", "_hh", " ", "n_", "4", 
     "_l", "_", "10", "_u", "_", "1"}], ",", "2", ",", "3", ",", "4", ",", 
    "5", ",", "6", ",", "7", ",", "8", ",", "9", ",", "10", ",", 
    RowBox[{"11", " ", "0.25949343335133"}]}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{"Simplify", "[", 
   RowBox[{"F", "=", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "0.428474"}], "*", 
      RowBox[{"(", 
       RowBox[{"HH", "-", 
        RowBox[{"(", 
         RowBox[{"-", "32.4182"}], ")"}]}], ")"}]}], "+", 
     RowBox[{"0.0900563", "*", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"(", 
            RowBox[{"DL", "+", 
             RowBox[{"(", 
              RowBox[{"DL", "-", 
               RowBox[{"(", "64.3442", ")"}]}], ")"}]}], ")"}], ")"}], "^", 
          "2"}], ")"}], ")"}], "^", 
       RowBox[{"(", 
        RowBox[{"1", "/", "2"}], ")"}]}]}], "+", 
     RowBox[{"4.6375", "*", 
      RowBox[{"(", 
       RowBox[{"(", 
        RowBox[{"(", 
         RowBox[{"Cos", "[", 
          RowBox[{
           RowBox[{"(", 
            RowBox[{
             RowBox[{"(", 
              RowBox[{"(", 
               RowBox[{"HH", "-", 
                RowBox[{"(", 
                 RowBox[{"-", "32.4182"}], ")"}]}], ")"}], ")"}], "^", 
             RowBox[{"(", 
              RowBox[{"1", "/", "2"}], ")"}]}], ")"}], "^", 
           RowBox[{"(", 
            RowBox[{"1", "/", "2"}], ")"}]}], "]"}], ")"}], ")"}], ")"}]}], 
     "+", 
     RowBox[{"2.11469", "*", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"(", 
         RowBox[{"DL", "*", 
          RowBox[{"(", "64.3442", ")"}]}], ")"}], ")"}], "^", 
       RowBox[{"(", 
        RowBox[{"1", "/", "2"}], ")"}]}]}]}]}], "]"}]}]], "Input",
 CellLabel->"In[40]:=",ExpressionUUID->"ed7eb35a-e409-40d2-8a08-0aa15b73d504"],

Cell[BoxData[
 RowBox[{
  RowBox[{"16.962951266776003`", " ", 
   SqrtBox["DL"]}], "+", 
  RowBox[{"0.0900563`", " ", 
   SqrtBox[
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "64.3442`"}], "+", 
       RowBox[{"2", " ", "DL"}]}], ")"}], "2"]]}], "-", 
  RowBox[{"0.428474`", " ", 
   RowBox[{"(", 
    RowBox[{"32.4182`", "\[VeryThinSpace]", "+", "HH"}], ")"}]}], "+", 
  RowBox[{"4.6375`", " ", 
   RowBox[{"Cos", "[", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"32.4182`", "\[VeryThinSpace]", "+", "HH"}], ")"}], 
     RowBox[{"1", "/", "4"}]], "]"}]}]}]], "Output",
 CellChangeTimes->{3.832567497955768*^9},
 CellLabel->"Out[40]=",ExpressionUUID->"89b6004c-3e27-4fb1-8543-0e06a93c5b06"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
    "res_", "3", " ", "simulans", " ", "atonal_dl", "_hh", " ", "n_", "6", 
     "_l", "_", "10", "_u", "_", "1"}], ",", "2", ",", "3", ",", "4", ",", 
    "5", ",", "7", ",", "8", ",", 
    RowBox[{"9", " ", "0.24383032909237679"}]}], "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{"Simplify", "[", 
   RowBox[{
    RowBox[{"2.87285", "*", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"(", 
        RowBox[{"DL", "-", 
         RowBox[{"(", 
          RowBox[{"-", "58.8634"}], ")"}]}], ")"}], ")"}], "^", 
      RowBox[{"(", 
       RowBox[{"1", "/", "2"}], ")"}]}]}], "-", 
    RowBox[{"0.709201", "*", "HH"}], "+", 
    RowBox[{"4.5857", "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"(", "HH", ")"}], "^", 
       RowBox[{"(", 
        RowBox[{"1", "/", "2"}], ")"}]}], ")"}]}], "-", 
    RowBox[{"7.77513", "*", 
     RowBox[{"(", "0", ")"}]}], "+", 
    RowBox[{"8.3948", "*", "DL"}], "-", 
    RowBox[{"7.37187", "*", 
     RowBox[{"(", 
      RowBox[{"(", 
       RowBox[{"(", 
        RowBox[{"(", 
         RowBox[{"(", 
          RowBox[{"(", 
           RowBox[{"(", "DL", ")"}], ")"}], ")"}], ")"}], ")"}], ")"}], 
      ")"}]}]}], "]"}]}]], "Input",
 CellLabel->"In[41]:=",ExpressionUUID->"11fe5744-cc89-4c8b-84ef-333110a1c78d"],

Cell[BoxData[
 RowBox[{
  RowBox[{"1.0229299999999997`", " ", "DL"}], "+", 
  RowBox[{"2.87285`", " ", 
   SqrtBox[
    RowBox[{"58.8634`", "\[VeryThinSpace]", "+", "DL"}]]}], "+", 
  RowBox[{"4.5857`", " ", 
   SqrtBox["HH"]}], "-", 
  RowBox[{"0.709201`", " ", "HH"}]}]], "Output",
 CellChangeTimes->{3.832567529414915*^9},
 CellLabel->"Out[41]=",ExpressionUUID->"9c79173c-3d4c-4fa2-b1aa-999130f292af"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
    "res_", "3", " ", "simulans", " ", "atonal_dl", "_hh", " ", "n_", "6", 
     "_l", "_", "10", "_u", "_", "1"}], ",", "2", ",", "3", ",", "4", ",", 
    RowBox[{"5", " ", "0.2465371839376529"}]}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{"Simplify", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"-", "0.46937"}], "*", 
     RowBox[{"(", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"DL", "-", 
          RowBox[{"(", 
           RowBox[{"-", "8.13329"}], ")"}]}], ")"}], "+", 
        RowBox[{"(", 
         RowBox[{"(", 
          RowBox[{"(", "DL", ")"}], ")"}], ")"}]}], ")"}], ")"}]}], "-", 
    RowBox[{"4.41266", "*", 
     RowBox[{"(", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"DL", "-", 
          RowBox[{"(", 
           RowBox[{"-", "8.13329"}], ")"}]}], ")"}], "-", 
        RowBox[{"(", 
         RowBox[{"DL", "-", 
          RowBox[{"(", 
           RowBox[{"-", "8.13329"}], ")"}]}], ")"}]}], ")"}], ")"}]}], "-", 
    RowBox[{"2.82144", "*", 
     RowBox[{"(", 
      RowBox[{"(", 
       RowBox[{"(", 
        RowBox[{"DL", "-", 
         RowBox[{"(", 
          RowBox[{"-", "8.13329"}], ")"}]}], ")"}], ")"}], ")"}]}], "-", 
    RowBox[{"4.20143", "*", 
     RowBox[{"(", 
      RowBox[{"(", 
       RowBox[{"(", 
        RowBox[{"DL", "-", 
         RowBox[{"(", 
          RowBox[{"-", "8.13329"}], ")"}]}], ")"}], ")"}], ")"}]}], "+", 
    RowBox[{"9.01872", "*", 
     RowBox[{"(", 
      RowBox[{"(", 
       RowBox[{"(", 
        RowBox[{"(", 
         RowBox[{"DL", "-", 
          RowBox[{"(", 
           RowBox[{"-", "8.13329"}], ")"}]}], ")"}], ")"}], ")"}], ")"}]}], 
    "-", 
    RowBox[{"0.442294", "*", 
     RowBox[{"(", 
      RowBox[{"(", 
       RowBox[{"(", 
        RowBox[{"HH", "-", 
         RowBox[{"(", "73.1792", ")"}]}], ")"}], ")"}], ")"}]}]}], 
   "]"}]}]], "Input",
 CellLabel->"In[42]:=",ExpressionUUID->"20335eb8-a9c1-4d88-b2e6-8734d49358b3"],

Cell[BoxData[
 RowBox[{"44.782025604`", "\[VeryThinSpace]", "+", 
  RowBox[{"1.0571099999999998`", " ", "DL"}], "-", 
  RowBox[{"0.442294`", " ", "HH"}]}]], "Output",
 CellChangeTimes->{3.8325675527731123`*^9},
 CellLabel->"Out[42]=",ExpressionUUID->"281d3a23-92d0-4b34-a6ed-acc1e1456378"]
}, Open  ]]
},
WindowSize->{1280, 941},
WindowMargins->{{-8, Automatic}, {Automatic, -8}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 1516, 46, 64, "Input",ExpressionUUID->"5762425e-08db-4ae7-8c51-85c494b56762"],
Cell[2099, 70, 372, 7, 36, "Output",ExpressionUUID->"dac3aa63-4b14-4544-a9df-87b0a80aaf7b"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2508, 82, 1170, 33, 64, "Input",ExpressionUUID->"9dbcab22-48fc-45cc-bc80-183642ba2ed0"],
Cell[3681, 117, 285, 5, 32, "Output",ExpressionUUID->"819999e8-0d33-49fa-895a-390c3da4ac1c"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4003, 127, 1784, 56, 85, "Input",ExpressionUUID->"4c6b3b12-ece4-4aa1-aa98-cf48a22a223f"],
Cell[5790, 185, 358, 7, 36, "Output",ExpressionUUID->"6495afac-bc4e-4560-af63-d00a5b9b1c02"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6185, 197, 1986, 61, 101, "Input",ExpressionUUID->"d42d2d39-1da7-4f8f-a22a-ec10918f884a"],
Cell[8174, 260, 840, 24, 59, "Output",ExpressionUUID->"b7ec6187-75e4-4f64-95ac-b1a0f9c09cc5"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9051, 289, 2221, 67, 101, "Input",ExpressionUUID->"02d6d98d-7561-41ab-b0a2-524c0cf70e09"],
Cell[11275, 358, 670, 16, 55, "Output",ExpressionUUID->"636aa9b1-5b1d-4621-aee9-eed6275f4195"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11982, 379, 2414, 72, 101, "Input",ExpressionUUID->"252b5eaa-d275-4c90-80e4-950919d16650"],
Cell[14399, 453, 994, 29, 93, "Output",ExpressionUUID->"4a0cf354-3c9f-49c7-8229-5a618574179a"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15430, 487, 1431, 43, 64, "Input",ExpressionUUID->"6a196b06-2975-4669-8325-a85d353778ca"],
Cell[16864, 532, 406, 9, 55, "Output",ExpressionUUID->"8056317a-4160-4cea-b911-7ca193d18bea"]
}, Open  ]],
Cell[CellGroupData[{
Cell[17307, 546, 1872, 57, 85, "Input",ExpressionUUID->"b90a0a5f-05b5-4570-94bd-65b00d834bbb"],
Cell[19182, 605, 598, 17, 40, "Output",ExpressionUUID->"dca80c72-baee-4b73-bf44-904060406abb"]
}, Open  ]],
Cell[CellGroupData[{
Cell[19817, 627, 1759, 52, 64, "Input",ExpressionUUID->"74120d0c-a50d-4858-ab81-d8dbedf70b61"],
Cell[21579, 681, 278, 5, 32, "Output",ExpressionUUID->"29d34aaa-9372-4a22-a1cf-1a44aa5c3bd5"]
}, Open  ]],
Cell[CellGroupData[{
Cell[21894, 691, 2246, 69, 101, "Input",ExpressionUUID->"591372c0-44ff-4828-85c8-990cb169ddc5"],
Cell[24143, 762, 1122, 31, 63, "Output",ExpressionUUID->"25202c90-b754-44b3-9b05-142023a6fc26"]
}, Open  ]],
Cell[CellGroupData[{
Cell[25302, 798, 1968, 58, 85, "Input",ExpressionUUID->"ed7eb35a-e409-40d2-8a08-0aa15b73d504"],
Cell[27273, 858, 730, 21, 45, "Output",ExpressionUUID->"89b6004c-3e27-4fb1-8543-0e06a93c5b06"]
}, Open  ]],
Cell[CellGroupData[{
Cell[28040, 884, 1321, 40, 64, "Input",ExpressionUUID->"11fe5744-cc89-4c8b-84ef-333110a1c78d"],
Cell[29364, 926, 404, 10, 36, "Output",ExpressionUUID->"9c79173c-3d4c-4fa2-b1aa-999130f292af"]
}, Open  ]],
Cell[CellGroupData[{
Cell[29805, 941, 2031, 64, 85, "Input",ExpressionUUID->"20335eb8-a9c1-4d88-b2e6-8734d49358b3"],
Cell[31839, 1007, 289, 5, 32, "Output",ExpressionUUID->"281d3a23-92d0-4b34-a6ed-acc1e1456378"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

